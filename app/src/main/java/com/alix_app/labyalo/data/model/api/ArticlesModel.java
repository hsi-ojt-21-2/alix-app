package com.alix_app.labyalo.data.model.api;

import com.alix_app.labyalo.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ArticlesModel extends AndroidModel {

    @SerializedName("headline")
    public String headline = "No data available";

    @SerializedName("subtext")
    public String subtext = "No data available";

    @SerializedName("link")
    public String link;

    @SerializedName("previewIMG")
    public String previewIMG;

    @SerializedName("date")
    public String date = "No data available";

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ArticlesModel convertFromJson(String json) {

        return convertFromJson(json, ArticlesModel.class);
    }

    public ArticlesModel(String headline, String subtext, String link, String previewIMG, String date) {
        if (!headline.equals("") || !subtext.equals("") || !link.equals("") || !previewIMG.equals("") || !date.equals("")) {
            this.headline = headline;
            this.subtext = subtext;
            this.link = link;
            this.previewIMG = previewIMG;
            this.date = date;
        }
    }
}
