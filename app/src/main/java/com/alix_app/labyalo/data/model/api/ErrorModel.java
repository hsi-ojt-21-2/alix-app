package com.alix_app.labyalo.data.model.api;

import com.alix_app.labyalo.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ErrorModel extends AndroidModel {

    @SerializedName("name")
    public String name;
    @SerializedName("fname")
    public List<String> fname;
    @SerializedName("lname")
    public List<String> lname;
    @SerializedName("contact_number")
    public List<String> contactNumber;
    @SerializedName("street_address")
    public List<String> streetAddress;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ErrorModel convertFromJson(String json) {
        return convertFromJson(json, ErrorModel.class);
    }

}
