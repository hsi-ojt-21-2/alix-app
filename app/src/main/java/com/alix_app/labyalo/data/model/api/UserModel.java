package com.alix_app.labyalo.data.model.api;

import com.alix_app.labyalo.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UserModel extends AndroidModel {
    @SerializedName("name")
    public String name;
    /**
     * fname : Elijah Alixtair
     * lname : Estolas
     * contact_number : +639958707853
     * overall_ongoing : 2
     * overall_completed : 0
     * this_week_completed : 0
     * today_completed : 0
     * member_since : {"date_db":"2021-02-15","month_year":"Feb 2021","time_passed":"22 hours ago","timestamp":{"date":"2021-02-15 17:41:57.000000","timezone_type":3,"timezone":"Asia/Manila"}}
     * avatar : {"data":{"path":"","filename":"","directory":"","full_path":"http://internal.san-luis.highlysucceed.com/resized","thumb_path":"http://internal.san-luis.highlysucceed.com/thumbnails"}}
     */

    @SerializedName("fname")
    public String fname;
    @SerializedName("lname")
    public String lname;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("overall_ongoing")
    public int overallOngoing;
    @SerializedName("overall_completed")
    public int overallCompleted;
    @SerializedName("this_week_completed")
    public int thisWeekCompleted;
    @SerializedName("today_completed")
    public int todayCompleted;
    @SerializedName("member_since")
    public MemberSinceBean memberSince;
    @SerializedName("avatar")
    public AvatarBean avatar;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public UserModel convertFromJson(String json) {
        return convertFromJson(json, UserModel.class);
    }

    public static class MemberSinceBean{
        /**
         * date_db : 2021-02-15
         * month_year : Feb 2021
         * time_passed : 22 hours ago
         * timestamp : {"date":"2021-02-15 17:41:57.000000","timezone_type":3,"timezone":"Asia/Manila"}
         */

        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public TimestampBean timestamp;

        public static class TimestampBean{
            /**
             * date : 2021-02-15 17:41:57.000000
             * timezone_type : 3
             * timezone : Asia/Manila
             */

            @SerializedName("date")
            public String date;
            @SerializedName("timezone_type")
            public int timezoneType;
            @SerializedName("timezone")
            public String timezone;
        }
    }

    public static class AvatarBean{
        /**
         * data : {"path":"","filename":"","directory":"","full_path":"http://internal.san-luis.highlysucceed.com/resized","thumb_path":"http://internal.san-luis.highlysucceed.com/thumbnails"}
         */

        @SerializedName("data")
        public DataBean data;

        public static class DataBean{
            /**
             * path :
             * filename :
             * directory :
             * full_path : http://internal.san-luis.highlysucceed.com/resized
             * thumb_path : http://internal.san-luis.highlysucceed.com/thumbnails
             */

            @SerializedName("path")
            public String path;
            @SerializedName("filename")
            public String filename;
            @SerializedName("directory")
            public String directory;
            @SerializedName("full_path")
            public String fullPath;
            @SerializedName("thumb_path")
            public String thumbPath;
        }
    }
}
