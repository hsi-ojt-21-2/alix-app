package com.alix_app.labyalo.data.model.api;

import com.alix_app.labyalo.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MomentModel extends AndroidModel {

    @SerializedName("name")
    public String name;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public MomentModel convertFromJson(String json) {
        return convertFromJson(json, MomentModel.class);
    }
}
