package com.alix_app.labyalo.config;

/**
 * Created by Labyalo on 8/7/2017.
 */

public class Keys {
    public static final String DEVICE_ID = "device_id";
    public static final String DEVICE_NAME = "device_name";
    public static final String DEVICE_REG_ID = "device_reg_id";
    public static final String PAGE = "page";
    public static final String PER_PAGE = "per_page";
    public static final String USER_ID = "user_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String INCLUDE = "include";
    public static final String FIRST_NAME = "fname";
    public static final String LAST_NAME = "lname";
    public static final String STREET_ADDRESS = "street_address";
    public static final String CONTACT_NUMBER = "contact_number";
}
