package com.alix_app.labyalo.config;

import com.alix_app.labyalo.vendor.android.java.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {
    public static final String PRODUCTION_URL = decrypt("http://internal.san-luis.highlysucceed.com/");
    public static final String DEBUG_URL = decrypt("http://internal.san-luis.highlysucceed.com/");
    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;
    public static final String getLogin() {return "/api/auth/login.json";}
    public static final String getRegister() {return "/api/auth/register.json";}
}
