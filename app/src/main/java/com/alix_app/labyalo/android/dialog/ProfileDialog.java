package com.alix_app.labyalo.android.dialog;

import android.widget.ImageView;
import android.widget.TextView;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.data.model.api.ArticlesModel;
import com.alix_app.labyalo.vendor.android.base.BaseDialog;

import javax.security.auth.callback.Callback;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProfileDialog extends BaseDialog {
	public static final String TAG = ProfileDialog.class.getName().toString();

	private ArticlesModel articlesModel;
	Callback callback;

	public static ProfileDialog newInstance() {
		ProfileDialog dialog = new ProfileDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_profile;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@BindView(R.id.closeBTN)
	ImageView closeBTN;
	@OnClick(R.id.closeBTN)
	void closeBTN(){
		dismiss();
	}
}
