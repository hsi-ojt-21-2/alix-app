package com.alix_app.labyalo.android.dialog;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.vendor.android.base.BaseDialog;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DefaultDialog extends BaseDialog {
	public static final String TAG = DefaultDialog.class.getName().toString();

	public static DefaultDialog newInstance() {
		DefaultDialog dialog = new DefaultDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_profile;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
}
