package com.alix_app.labyalo.android.fragment.register;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.activity.RegisterActivity;
import com.alix_app.labyalo.android.dialog.SignupSuccessDialog;
import com.alix_app.labyalo.data.model.api.UserModel;
import com.alix_app.labyalo.server.request.Auth;
import com.alix_app.labyalo.vendor.android.base.BaseFragment;
import com.alix_app.labyalo.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignUpFragment extends BaseFragment implements SignupSuccessDialog.Callback {
    public static final String TAG = SignUpFragment.class.getName().toString();
    private RegisterActivity registerActivity;


    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @BindView(R.id.signUpBTN)                       TextView signUpBTN;
    @BindView(R.id.loginBTN)            TextView loginBTN;
    @BindView(R.id.firstnameET)   EditText firstnameET;
    @BindView(R.id.lastnameET)    EditText lastnameET;
    @BindView(R.id.contactET)  EditText contactET;
    @BindView(R.id.addressET)  EditText addressET;
    @BindView(R.id.firstnameWarningTV)  TextView firstnameWarningTV;
    @BindView(R.id.lastnameWarningTV)   TextView lastnameWarningTV;
    @BindView(R.id.contactWarningTV)    TextView contactWarningTV;
    @BindView(R.id.addressWarningTV)    TextView addressWarningTV;




    @OnTextChanged(R.id.firstnameET)
    void firstnameET(){
        if(firstnameET.getText().toString().equals("") || checkIllegalCharacters(firstnameET.getText().toString(),"name")){
            System.out.println("fname text is empty: "+firstnameET.getText().toString().equals(""));
            System.out.println("fname text is illegal: "+checkIllegalCharacters(firstnameET.getText().toString(),"name"));
            firstnameWarningTV.setVisibility(View.VISIBLE);
        }
        else{
            System.out.println("fname text is empty: "+firstnameET.getText().toString().equals(""));
            System.out.println("fname text is illegal: "+checkIllegalCharacters(firstnameET.getText().toString(),"name"));
            firstnameWarningTV.setVisibility(View.GONE);
        }
    }
    @OnTextChanged(R.id.lastnameET)
    void lastnameET(){
        if(lastnameET.getText().toString().equals("") || checkIllegalCharacters(lastnameET.getText().toString(),"name")){
            lastnameWarningTV.setVisibility(View.VISIBLE);
        }
        else{
            lastnameWarningTV.setVisibility(View.GONE);
        }
    }
    @OnTextChanged(R.id.contactET)
    void contactET(){
        if(contactET.getText().toString().equals("") || checkIllegalCharacters(contactET.getText().toString(),"contact")){
            contactWarningTV.setVisibility(View.VISIBLE);
        }
        else{
            contactWarningTV.setVisibility(View.GONE);
        }
    }
    @OnTextChanged(R.id.addressET)
    void addressET() {
        if (addressET.getText().toString().equals("")) {
            addressWarningTV.setVisibility(View.VISIBLE);
        }
        else{
            addressWarningTV.setVisibility(View.GONE);
        }
    }
    private boolean checkIllegalCharacters(String entry, String tag) {
        switch (tag) {
            case "name": {
                System.out.println("entry: "+entry);
                entry = entry.toLowerCase();
                char[] charArray = entry.toCharArray();
                boolean illegal = true;
                for (int i = 0; i < charArray.length; i++) {
                    char ch = charArray[i];
                    if ((ch >= 'a' && ch <= 'z')||ch == ' ') {
                        illegal = false;
                        System.out.println(ch+" character illegal: "+illegal);
                    } else {
                        illegal = true;
                        System.out.println(ch+" character illegal: "+illegal);
                    }
                }
                System.out.println(entry + " is illegal: " + illegal+" returned");
                return illegal;
            }

            case "contact": {
                if (entry.length() < 11) {
                    System.out.println("contact is less than 11: " + (entry.length() < 11));
                    return true;
                }if (entry.length() > 12){
                    return true;
                }
                else {
                    String entryPrefix = entry.substring(0, 2);
                    if (entryPrefix.equals("08") || entryPrefix.equals("09")) {
                        System.out.println("contact prefix 08or09: true");
                        entryPrefix = entry.substring(0, 4);
                        int entryPrefixInt = Integer.parseInt(entryPrefix);
                        System.out.println("entryPrefixInt value: " + entryPrefixInt);
                        if (entryPrefix.equals("0817") ||
                                (entryPrefixInt >= 905 && entryPrefixInt <= 910) ||
                                (entryPrefixInt == 912) ||
                                (entryPrefixInt >= 915 && entryPrefixInt <= 943) ||  //a fucking radix tree wouldve been better you goddamn mong
                                (entryPrefixInt >= 945 && entryPrefixInt <= 951) ||
                                (entryPrefixInt >= 953 && entryPrefixInt <= 956) || (entryPrefixInt == 961) ||
                                (entryPrefixInt >= 965 && entryPrefixInt <= 967) ||
                                (entryPrefixInt >= 973 && entryPrefixInt <= 975) ||
                                (entryPrefixInt >= 977 && entryPrefixInt <= 979) ||
                                (entryPrefixInt >= 995 && entryPrefixInt <= 999)) {
                            System.out.println("contact prefix is all legal");
                            return false;
                        } else {
                            System.out.println("contact prefix first 4 nums are legal: true");
                            entryPrefix = entry.substring(0, 5);
                            entryPrefixInt = Integer.parseInt(entryPrefix);
                            if ((entryPrefixInt >= 9173 && entryPrefixInt <= 9178 && entryPrefixInt != 9174 && entryPrefixInt != 9177) ||
                                    (entryPrefixInt >= 9253 && entryPrefixInt <= 9258 && entryPrefixInt != 9254)) {
                                System.out.println("contact prefix is all legal");
                                return false;
                            } else {
                                System.out.println("contact prefix is all ILLEGAL");
                                return true;
                            }

                        }

                    } else {

                        System.out.println("contact prefix 08or09: false");
                        return true;
                    }
                }
            }
            default:
            return true;
        }
    }

    @Override
    public int onLayoutSet() {
        //put context sensitive methods in "onViewReady"
        return R.layout.articles_signup; //ACTIVITY LAYOUTS ARE THE HEADERS
        //FRAGMENT LAYOUTS ARE THE CONTENT
        //POG
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
    }


    @Override
    public void onResume() {
        super.onResume();
        registerActivity.setTitle("Sign Up");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onResponse(Auth.RegisterResponse registerResponse){
        SingleTransformer<UserModel>  transformer = registerResponse.getData(SingleTransformer.class);
        if(transformer.status){

                SignupSuccessDialog.newInstance(registerActivity, this).show(getFragmentManager(), TAG);

        }else{
            Toast.makeText(getContext(),"Phone number already used.", Toast.LENGTH_LONG).show();
            System.out.println("Phone number sdfmgsdfklg toast");
            }




        Log.d("Register Response", String.valueOf(transformer.msg));
    }
    @OnClick(R.id.loginBTN)
    void loginBTN() {
        System.out.println("signUpBTN pressed");
        registerActivity.startLandingActivity("");
    }


    @OnClick(R.id.signUpBTN)
    void onClick() {


        if ((firstnameWarningTV.getVisibility() == View.GONE) &&
                (lastnameWarningTV.getVisibility() == View.GONE) &&
                (contactWarningTV.getVisibility() == View.GONE) &&
                (addressWarningTV.getVisibility() == View.GONE) &&
                !firstnameET.getText().toString().equals("") &&
                !lastnameET.getText().toString().equals("") &&
                !contactET.getText().toString().equals("") &&
                !addressET.getText().toString().equals("")
        ) {
             Auth.getDefault().signup(
                    registerActivity,firstnameET.getText().toString(),
                    lastnameET.getText().toString(),
                    contactET.getText().toString(),
                    addressET.getText().toString());
        }else{
            System.out.println(((firstnameWarningTV.getVisibility() == View.GONE) &&
                    (lastnameWarningTV.getVisibility() == View.GONE) &&
                    (contactWarningTV.getVisibility() == View.GONE) &&
                    (addressWarningTV.getVisibility() == View.GONE) &&
                    !firstnameET.getText().toString().equals("") &&
                    !lastnameET.getText().toString().equals("") &&
                    !contactET.getText().toString().equals("") &&
                    !"".equals(addressET.getText().toString()))+" signupBTN check result");
            System.out.println("firstnameWarningTV.getVisibility() == View.INVISIBLE): "+(firstnameWarningTV.getVisibility() == View.GONE));
            System.out.println("lastnameWarningTV.getVisibility() == View.INVISIBLE): "+(lastnameWarningTV.getVisibility() == View.GONE));
            System.out.println("addressWarningTV.getVisibility() == View.INVISIBLE): "+(addressWarningTV.getVisibility() == View.GONE));
            System.out.println("contactWarningTV.getVisibility() == View.INVISIBLE): "+(contactWarningTV.getVisibility() == View.GONE));
            System.out.println("!firstnameET.getText().toString().equals(\"\"): "+(!firstnameET.getText().toString().equals("")));
            System.out.println("!lastnameET.getText().toString().equals(\"\")): "+(!lastnameET.getText().toString().equals("")));
            System.out.println("!contactET.getText().toString().equals(\"\"): "+(!contactET.getText().toString().equals("")));
            System.out.println("!\"\".equals(addressET.getText().toString())): "+(!"".equals(addressET.getText().toString())));


        }
    }

    @BindView(R.id.activityBackBTN)
    ImageView activityBackBTN;
    @OnClick(R.id.activityBackBTN)
    void activityBackBTN(){
        registerActivity.finish();
    }

    @Override
    public void onSuccess(boolean isSuccess) {
        registerActivity.startLandingActivity("");
    }
}
