package com.alix_app.labyalo.android.activity;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.fragment.DefaultFragment;
import com.alix_app.labyalo.android.fragment.login.LoginFragment;
import com.alix_app.labyalo.android.fragment.register.SignUpFragment;
import com.alix_app.labyalo.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RegisterActivity extends RouteActivity {
    public static final String TAG = RegisterActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.blank;  //ACTIVITY LAYOUTS ARE THE HEADERS
                                //FRAGMENT LAYOUTS ARE THE CONTENT
                                //POG
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "signup":
                openSignUpFragment();
                break;
            case "login":
                openLoginFragment();
            default:
                break;
        }
    }



    public void openLoginFragment() {switchFragment(LoginFragment.newInstance());}
    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openSignUpFragment(){ switchFragment(SignUpFragment.newInstance()); }


}
