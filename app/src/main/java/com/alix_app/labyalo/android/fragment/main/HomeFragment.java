package com.alix_app.labyalo.android.fragment.main;

import android.util.Log;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.activity.MainActivity;
import com.alix_app.labyalo.server.request.Auth;
import com.alix_app.labyalo.vendor.android.base.BaseFragment;
import com.alix_app.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HomeFragment extends BaseFragment {
    public static final String TAG = HomeFragment.class.getName().toString();

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    private MainActivity mainActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_default;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setSelectedItem("home");
        mainActivity.setTitle("Home");
        mainActivity.homeActive();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
