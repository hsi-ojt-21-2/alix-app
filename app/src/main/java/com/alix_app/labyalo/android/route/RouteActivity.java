package com.alix_app.labyalo.android.route;

import android.content.Intent;
import android.os.Bundle;

import com.alix_app.labyalo.android.activity.ArticlesActivity;
import com.alix_app.labyalo.android.activity.LandingActivity;
import com.alix_app.labyalo.android.activity.MainActivity;
import com.alix_app.labyalo.android.activity.ProfileActivity;
import com.alix_app.labyalo.android.activity.RegisterActivity;
import com.alix_app.labyalo.vendor.android.base.BaseActivity;
import com.alix_app.labyalo.vendor.android.base.RouteManager;

/**
 * Created by Labyalo on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

    public void startMainActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
    }

    public void startRegisterActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(RegisterActivity.class)
                .addActivityTag("register")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startLandingActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(LandingActivity.class)
                .addActivityTag("landing")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    public void startProfileActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(ProfileActivity.class)
                .addActivityTag("cash_in")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startArticleActivity(String fragmentTAG){
        System.out.println("RouteActivity startArticleActivity");
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(ArticlesActivity.class)  //mind the activity class you're using when copy
                .addActivityTag("articles")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }


//    @Subscribe
//    public void invalidToken(InvalidToken invalidToken){
//        Log.e("Token", "Expired");
//        EventBus.getDefault().unregister(this);
//        UserData.insert(new UserModel());
//        UserData.insert(UserData.TOKEN_EXPIRED, true);
//        startLandingActivity();
//    }
}
