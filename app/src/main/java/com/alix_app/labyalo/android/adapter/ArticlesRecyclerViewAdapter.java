package com.alix_app.labyalo.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.data.model.api.ArticlesModel;
import com.alix_app.labyalo.data.model.api.SampleModel;
import com.alix_app.labyalo.vendor.android.base.BaseRecylerViewAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ArticlesRecyclerViewAdapter extends BaseRecylerViewAdapter<ArticlesRecyclerViewAdapter.ViewHolder, ArticlesModel>{

    private ClickListener clickListener;

    public ArticlesRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_articles));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));


        Glide.with(getContext())
                .load(holder.getItem().previewIMG)
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.loadingimage)
                        .error(R.drawable.icon_failed))
                .into(holder.imageIV);

        holder.headlineTV.setText(holder.getItem().headline);
        holder.subtextTV.setText(holder.getItem().subtext);
        holder.dateTV.setText(holder.getItem().date);

        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)  View adapterCON;
        @BindView(R.id.imageIV)  ImageView imageIV;
        @BindView(R.id.headlineTV)     TextView headlineTV;
        @BindView(R.id.subtextTV)   TextView subtextTV;
        @BindView(R.id.dateTV)    TextView dateTV;
        public ViewHolder(View view) {
            super(view);
        }

        public ArticlesModel getItem() {
            return (ArticlesModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.adapterCON:
                if (clickListener != null) {
                    clickListener.onItemClick((ArticlesModel) v.getTag());
                }
                break;
        }
    }

    public interface ClickListener{
        void onItemClick(ArticlesModel articlesModels);
    }
} 
