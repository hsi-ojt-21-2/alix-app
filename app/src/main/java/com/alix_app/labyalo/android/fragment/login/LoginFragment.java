package com.alix_app.labyalo.android.fragment.login;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.activity.LandingActivity;
import com.alix_app.labyalo.data.model.api.UserModel;
import com.alix_app.labyalo.data.preference.UserData;
import com.alix_app.labyalo.server.request.Auth;
import com.alix_app.labyalo.vendor.android.base.BaseFragment;
import com.alix_app.labyalo.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnTextChanged;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = LoginFragment.class.getName().toString();

    private LandingActivity landingActivity;

    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.passET)              EditText passET;
    @BindView(R.id.loginBTN)            TextView loginBTN;
    @BindView(R.id.signUpBTN)           TextView signUpBTN;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {

        return R.layout.articles_login; //ACTIVITY LAYOUTS ARE THE HEADERS
        //FRAGMENT LAYOUTS ARE THE CONTENT
        //POG

    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        loginBTN.setOnClickListener(this);
        signUpBTN.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse) {
        SingleTransformer<UserModel> transformer = loginResponse.getData(SingleTransformer.class);
        if (String.valueOf(transformer.data).equals("null")) {
            Toast.makeText(getContext(), "Incorrect user credentials", Toast.LENGTH_LONG).show();
            System.out.println("Registration Incorrect Credentials Error");
        } else {
            if (transformer.status) {

                System.out.println("transformer.status == true String.valueOf(transformer.status_code): --->" + String.valueOf(transformer.status_code));
                UserData.insert(transformer.data);
                UserData.insert(UserData.AUTHORIZATION, transformer.token);
                landingActivity.startArticleActivity("articles");

            } else {
                Toast.makeText(getContext(), "Incorrect user credentials", Toast.LENGTH_LONG).show();
                System.out.println("transformer.status == false String.valueOf(transformer.status_code): --->" + String.valueOf(transformer.status_code));
            }

            Log.d("returnResponse", String.valueOf(transformer.data));
        }
    }
    @BindView(R.id.contactET) EditText contactET;
    @BindView(R.id.contactWarningTV) TextView contactWarningTV;
    @BindView(R.id.passWarningTV) TextView passWarningTV;

    @OnTextChanged(R.id.contactET)
    void contactET(){
        String contact = contactET.getText().toString();
        if(contact.equals("") || checkIllegalCharacters(contact,"contact")){
            contactWarningTV.setVisibility(View.VISIBLE);
        }else{
            contactWarningTV.setVisibility(View.GONE);
        }
    }

    @OnTextChanged(R.id.passET)
    void passET(){
        //String pass = passET.getText().toString();
        //if(pass.equals("") || pass.length() < 6){
        //    passWarningTV.setVisibility(View.VISIBLE);
        //}else{
        //    passWarningTV.setVisibility(View.GONE);
        //}
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBTN:
                System.out.println("loginBTN pressed");
                System.out.println("LoginFragment onClick getContext: "+getContext().toString());
                if(((!passET.getText().toString().equals("") && !contactET.getText().toString().equals("") ))&&
                        ((contactWarningTV.getVisibility() == View.GONE) && (passWarningTV.getVisibility() == View.GONE
                                )))
                {
                    System.out.println("passET.getText().toString().equals(\"\"): "+passET.getText().toString().equals(""));
                    System.out.println("contactET.getText().toString().equals(\"\")"+contactET.getText().toString().equals(""));
                    System.out.println("contactWarningTV.getVisibility() == View.GONE"+(contactWarningTV.getVisibility() == View.GONE));
                    System.out.println("passWarningTV.getVisibility() == View.GONE)"+(passWarningTV.getVisibility() == View.GONE));

                    Auth.getDefault().login(landingActivity,contactET.getText().toString(),passET.getText().toString());
                }else{
                    System.out.println("passET.getText().toString().equals(\"\"): "+!passET.getText().toString().equals(""));
                    System.out.println("contactET.getText().toString().equals(\"\")"+!contactET.getText().toString().equals(""));
                    System.out.println("contactWarningTV.getVisibility() == View.GONE"+(contactWarningTV.getVisibility() == View.GONE));
                    System.out.println("passWarningTV.getVisibility() == View.GONE)"+(passWarningTV.getVisibility() == View.GONE));
                    System.out.println("Toast message should be showing");
                    Toast.makeText(getContext(), "Please correctly fill out the required fields.", Toast.LENGTH_LONG).show();

                }
              break;
            case R.id.signUpBTN:
                System.out.println("signUpBTN pressed");
                landingActivity.startRegisterActivity("signup");
                break;



        }

    }
    private boolean checkIllegalCharacters(String entry, String tag) {
        switch (tag) {
            case "name": {
                System.out.println("entry: "+entry);
                entry = entry.toLowerCase();
                char[] charArray = entry.toCharArray();
                boolean illegal = true;
                for (int i = 0; i < charArray.length; i++) {
                    char ch = charArray[i];
                    if ((ch >= 'a' && ch <= 'z' )||ch == ' ') {
                        illegal = false;
                        System.out.println(ch+" character illegal: "+illegal);
                    } else {
                        illegal = true;
                        System.out.println(ch+" character illegal: "+illegal);
                    }
                }
                System.out.println(entry + " is illegal: " + illegal+" returned");
                return illegal;
            }

            case "contact": {
                if (entry.length() < 11) {
                    System.out.println("contact is less than 11: " + (entry.length() < 11));
                    return true;
                }if (entry.length() > 12){
                    return true;
                }
                else {
                    String entryPrefix = entry.substring(0, 2);
                    if (entryPrefix.equals("08") || entryPrefix.equals("09")) {
                        System.out.println("contact prefix 08or09: true");
                        entryPrefix = entry.substring(0, 4);
                        int entryPrefixInt = Integer.parseInt(entryPrefix);
                        System.out.println("entryPrefixInt value: " + entryPrefixInt);
                        if (entryPrefix.equals("0817") ||
                                (entryPrefixInt >= 905 && entryPrefixInt <= 910) ||
                                (entryPrefixInt == 912) ||
                                (entryPrefixInt >= 915 && entryPrefixInt <= 943) ||  //a fucking radix tree wouldve been better you goddamn mong
                                (entryPrefixInt >= 945 && entryPrefixInt <= 951) ||
                                (entryPrefixInt >= 953 && entryPrefixInt <= 956) || (entryPrefixInt == 961) ||
                                (entryPrefixInt >= 965 && entryPrefixInt <= 967) ||
                                (entryPrefixInt >= 973 && entryPrefixInt <= 975) ||
                                (entryPrefixInt >= 977 && entryPrefixInt <= 979) ||
                                (entryPrefixInt >= 995 && entryPrefixInt <= 999)) {
                            System.out.println("contact prefix is all legal");
                            return false;
                        } else {
                            System.out.println("contact prefix first 4 nums are legal: true");
                            entryPrefix = entry.substring(0, 5);
                            entryPrefixInt = Integer.parseInt(entryPrefix);
                            if ((entryPrefixInt >= 9173 && entryPrefixInt <= 9178 && entryPrefixInt != 9174 && entryPrefixInt != 9177) ||
                                    (entryPrefixInt >= 9253 && entryPrefixInt <= 9258 && entryPrefixInt != 9254)) {
                                System.out.println("contact prefix is all legal");
                                return false;
                            } else {
                                System.out.println("contact prefix is all ILLEGAL");
                                return true;
                            }

                        }

                    } else {

                        System.out.println("contact prefix 08or09: false");
                        return true;
                    }
                }
            }
            default:
                return true;
        }
    }
    // @OnClick(R.id.signUpBTN)
    // void signUpBTN() {
    //     System.out.println("signUpBTN pressed");
    //    landingActivity.startRegisterActivity("signup");
    //}
}
