package com.alix_app.labyalo.android.fragment.articles;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.activity.ArticlesActivity;
import com.alix_app.labyalo.android.adapter.ArticlesRecyclerViewAdapter;
import com.alix_app.labyalo.android.dialog.ArticleDialog;
import com.alix_app.labyalo.data.model.api.ArticlesModel;
import com.alix_app.labyalo.data.preference.UserData;
import com.alix_app.labyalo.server.request.Auth;
import com.alix_app.labyalo.vendor.android.base.BaseFragment;
import com.alix_app.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import javax.security.auth.callback.Callback;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Alix on 2/1/2021.
 */

public class ArticlesDefaultFragment extends BaseFragment implements ArticlesRecyclerViewAdapter.ClickListener, Callback {
    public static final String TAG = ArticlesDefaultFragment.class.getName().toString();

    ArticlesActivity articlesActivity;
    private ArticlesRecyclerViewAdapter articlesRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    ArrayList<ArticlesModel> articlesModels = new ArrayList<>();

    @BindView(R.id.activityTitleTXT)
    TextView activityTitleTXT;
    @BindView(R.id.articlesRV)
    RecyclerView articlesRV;
    @Override
    public void onItemClick(ArticlesModel articlesModels) {
        ArticleDialog.newInstance(articlesModels.link).show(getFragmentManager(), TAG);
    }

    public static ArticlesDefaultFragment newInstance() {
        System.out.println("ArticlesDefaultFragment newInstance");
        ArticlesDefaultFragment fragment = new ArticlesDefaultFragment();
        System.out.println("ArticlesDefaultFragment fragment = new ArticlesDefaultFragment(); success");
        System.out.println("fragment value:"+fragment.toString());
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        articlesActivity = (ArticlesActivity) getContext();
        return R.layout.articles_default;   //ACTIVITY LAYOUTS ARE THE HEADERS
        //FRAGMENT LAYOUTS ARE THE CONTENT
        //POG
    }

    @Override
    public void onViewReady() {
        activityTitleTXT.setText("Welcome, "+ UserData.getUserModel().fname+" "+UserData.getUserModel().lname);
        articlesActivity = (ArticlesActivity) getContext();
        setUpRecyclerview();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @BindView(R.id.menuBTN)ImageView menuBTN;
    @OnClick(R.id.menuBTN)
    void menuBTN(){
        System.out.println("ArticlesDefaultFragment menuBTN");
        System.out.println("context of menuBTN(): "+getContext().toString());
        articlesActivity = (ArticlesActivity) getContext();
        System.out.println("articlesActivity value:"+articlesActivity.getContext());
        articlesActivity.openSettingsFragment();

    }

    private void setUpRecyclerview() {
        articlesRecyclerViewAdapter= new ArticlesRecyclerViewAdapter(getContext());
        articlesModels.add(new ArticlesModel("PLDT seeks government help on fiber safety",
                "Telco giant PLDT Inc. is seeking cooperation from the government and other service providers to help in ensuring the safety of fiber cables from construction and excavation activities.",
                "https://www.philstar.com/business/2021/02/03/2074850/pldt-seeks-government-help-fiber-safety",
                "https://media.philstar.com/photos/2021/02/02/pldt_2021-02-02_18-17-31.jpg",
                "February 3, 2021"));
        articlesModels.add(new ArticlesModel("AMD rumored to outsource chip production to Samsung",
                "Samsung and AMD have been working closely on co-developing Exynos chipsets with AMD RDNA GPUs but according to new reports, the partnership may go up a notch.",
                "https://www.gsmarena.com/amd_rumored_to_outsource_chip_production_to_samsung-news-47509.php",
                "https://fdn.gsmarena.com/imgroot/news/21/02/amd-outsource-chip-production-samsung/-1200/gsmarena_002.jpg",
                "February 1, 2021"));
        articlesModels.add(new ArticlesModel("Parts of QC to experience water service interruption from Feb. 1 – Feb. 5 — Manila Water",
                "Some customers of Manila Water in Quezon City will experience water service interruption from Feb. 1 to Feb. 5.",
                "https://mb.com.ph/2021/02/02/parts-of-qc-to-experience-water-service-interruption-from-feb-1-feb-5-manila-water/",
                "https://mb.com.ph/wp-content/uploads/2020/09/manila-water.jpg",
                "February 2, 2021"));


        articlesRecyclerViewAdapter.setNewData(articlesModels);
        linearLayoutManager = new LinearLayoutManager(getContext());
        articlesRV.setLayoutManager(linearLayoutManager);
        articlesRV.setAdapter(articlesRecyclerViewAdapter);
        articlesRecyclerViewAdapter.setClickListener(this);
    }


}
