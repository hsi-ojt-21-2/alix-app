package com.alix_app.labyalo.android.activity;


import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.fragment.profile.UpdateProfileFragment;
import com.alix_app.labyalo.android.route.RouteActivity;

public class ProfileActivity extends RouteActivity {
    public static final String TAG = ProfileActivity.class.getName().toString();


    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {


        switch (fragmentName){
            case "update_profile":
                openUpdateProfileFragment();
                break;

        }
    }

    public void openUpdateProfileFragment(){ switchFragment(UpdateProfileFragment.newInstance()); }


}
