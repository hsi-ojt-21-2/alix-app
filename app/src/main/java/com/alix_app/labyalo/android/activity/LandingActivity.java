package com.alix_app.labyalo.android.activity;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.fragment.login.LoginFragment;
import com.alix_app.labyalo.android.fragment.landing.SplashFragment;
import com.alix_app.labyalo.android.fragment.register.SignUpFragment;
import com.alix_app.labyalo.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LandingActivity extends RouteActivity {
    public static final String TAG = LandingActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                openLoginFragment();
                break;
            default:
                openSplashFragment();
                break;
        }
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }
    public void openSignUpFragment(){ switchFragment(SignUpFragment.newInstance()); }
    public void openSplashFragment(){ switchFragment(SplashFragment.newInstance()); }
}
