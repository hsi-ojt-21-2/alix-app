package com.alix_app.labyalo.android.activity;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.fragment.articles.ArticlesDefaultFragment;
import com.alix_app.labyalo.android.fragment.DefaultFragment;
import com.alix_app.labyalo.android.fragment.main.SettingsFragment;
import com.alix_app.labyalo.android.route.RouteActivity;

/**
 * Created by Alix on 2/1/2021.
 */

public class ArticlesActivity extends RouteActivity {
    public static final String TAG = ArticlesActivity.class.getName().toString();


    @Override
    public int onLayoutSet() {
        System.out.println("ArticlesActivity onLayoutSet");
        return R.layout.blank;  //ACTIVITY LAYOUTS ARE THE HEADERS
        //FRAGMENT LAYOUTS ARE THE CONTENT
        //POG
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "articles":
                System.out.println("ArticlesActivity initialFragment articles");
                openArticlesDefaultFragment();
                break;
            default:
                System.out.println("ArticlesActivity initialFragment default");
                openDefaultFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openArticlesDefaultFragment(){
        System.out.println("ArticlesActivity openArticlesDefaultFragment");
        switchFragment(ArticlesDefaultFragment.newInstance()); }
    public void openSettingsFragment(){ switchFragment(SettingsFragment.newInstance()); }
}
