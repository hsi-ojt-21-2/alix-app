package com.alix_app.labyalo.android.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.data.model.api.ArticlesModel;
import com.alix_app.labyalo.vendor.android.base.BaseDialog;

import javax.security.auth.callback.Callback;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class ArticleDialog extends BaseDialog {
	public static final String TAG = ArticleDialog.class.getName().toString();


	private static String link;
	private ArticlesModel articlesModel;
	Callback callback;

	public static ArticleDialog newInstance(String url) {
		link = url;
		ArticleDialog dialog = new ArticleDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_article;
	}

	@Override
	public void onViewReady() {
		WebSettings webSettings = articleDialogWV.getSettings();
		webSettings.setJavaScriptEnabled(true);
		articleDialogWV.loadUrl(link);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}


	@BindView(R.id.articleDialogWV)
	WebView articleDialogWV;



	@BindView(R.id.closeBTN)
	ImageView closeBTN;
	@OnClick(R.id.closeBTN)
	void closeBTN(){
		dismiss();
	}
}
