package com.alix_app.labyalo.android.fragment.main;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;


import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.activity.ArticlesActivity;
import com.alix_app.labyalo.android.activity.MainActivity;
import com.alix_app.labyalo.android.dialog.DefaultDialog;
import com.alix_app.labyalo.android.dialog.LogoutDialog;
import com.alix_app.labyalo.android.dialog.ProfileDialog;
import com.alix_app.labyalo.server.request.Auth;
import com.alix_app.labyalo.vendor.android.base.BaseFragment;
import com.alix_app.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SettingsFragment extends BaseFragment implements LogoutDialog.Callback {
    public static final String TAG = SettingsFragment.class.getName().toString();
    private ArticlesActivity articlesActivity;
    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }



    @Override
    public int onLayoutSet() {
        return R.layout.articles_settings;  //ACTIVITY LAYOUTS ARE THE HEADERS
        //FRAGMENT LAYOUTS ARE THE CONTENT
        //POG
    }

    @Override
    public void onViewReady() {
        articlesActivity = (ArticlesActivity) getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        //articlesActivity.setSelectedItem("settings");
        articlesActivity.setTitle("Settings");
        //articlesActivity.settingActive();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @BindView(R.id.activityBackBTN)             ImageView activityBackBTN;
    @OnClick(R.id.activityBackBTN)
    void activityBackBTN(){

        articlesActivity.startArticleActivity("articles");
    }

    @BindView(R.id.logoutBTN)                   TextView logoutBTN;
    @OnClick(R.id.logoutBTN)
    void logoutBTNClicked(){
        LogoutDialog.newInstance(articlesActivity, this).show(getFragmentManager(), TAG);
    }

    @BindView(R.id.viewProfileBTN)                   TextView viewProfileBTN;
    @OnClick(R.id.viewProfileBTN)
    void viewProfileBTN(){
        ProfileDialog.newInstance().show(getFragmentManager(), TAG);
    }

    @Override
    public void onSuccess(boolean isSuccess) {
        articlesActivity.startLandingActivity("");
    }
}
