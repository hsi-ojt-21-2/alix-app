package com.alix_app.labyalo.android.dialog;

import android.content.Context;
import android.widget.TextView;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.activity.ArticlesActivity;
import com.alix_app.labyalo.data.model.api.ArticlesModel;
import com.alix_app.labyalo.data.preference.UserData;
import com.alix_app.labyalo.vendor.android.base.BaseDialog;

import javax.security.auth.callback.Callback;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class LogoutDialog extends BaseDialog {
	public static final String TAG = LogoutDialog.class.getName().toString();
	private Callback callback;
	private Context context;
	public static LogoutDialog newInstance(Context context, Callback callback) {
		LogoutDialog dialog = new LogoutDialog();
		dialog.callback = callback;
		dialog.context = context;
		return dialog;
	}

	@Override
	public int onLayoutSet() {


		return R.layout.dialog_logout;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@BindView(R.id.cancelBTN)
	TextView cancelBTN;
	@OnClick(R.id.cancelBTN)
	void cancelBTN(){
		dismiss();
		callback.onSuccess(true);
	}

	@BindView(R.id.confirmLogoutBTN)
	TextView confirmLogoutBTN;
	@OnClick(R.id.confirmLogoutBTN)
	void confirmLogoutBTN(){
		UserData.remove(UserData.getUserModel());
		//ArticlesActivity articlesActivity =(ArticlesActivity)getContext();
		//articlesActivity.startLandingActivity("");
		dismiss();
		callback.onSuccess(true);
	}
	public interface Callback{
		void onSuccess(boolean isSuccess);
	}
}
