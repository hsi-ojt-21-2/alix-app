package com.alix_app.labyalo.android.dialog;

import android.content.Context;
import android.widget.Button;
import android.widget.ImageView;

import com.alix_app.labyalo.R;
import com.alix_app.labyalo.android.activity.RegisterActivity;
import com.alix_app.labyalo.vendor.android.base.BaseDialog;

import javax.security.auth.callback.Callback;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignupSuccessDialog extends BaseDialog {
	public static final String TAG = SignupSuccessDialog.class.getName().toString();
	RegisterActivity registerActivity;
	private Context context;
	private Callback callback;

	public static SignupSuccessDialog newInstance(Context context, Callback callback) {
		SignupSuccessDialog dialog = new SignupSuccessDialog();
		dialog.context = context;
		dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_signup;
	}

	@Override
	public void onViewReady() {
		registerActivity = (RegisterActivity) getContext();
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@BindView(R.id.okayBTN)
	Button okayBTN;

	@OnClick(R.id.okayBTN)
	void okayBTN(){

		//registerActivity.startLandingActivity("");
		dismiss();
		callback.onSuccess(true);
	}

	@BindView(R.id.closeBTN)
	ImageView closeBTN;
	@OnClick(R.id.closeBTN)
	void closeBTN(){
		okayBTN();
	}


	public interface Callback{
		void onSuccess(boolean isSuccess);
	}

}
